clc; clear all;

lng_d= [12 11 11 11 12 13 13 13].*(pi/180);
lat_d= [54 54 55 56 56 56 55 54].*(pi/180);

lng_o=12*(pi/180);
lat_o=55*(pi/180);




heading_val=atan((lng_o-lng_d)./(lat_o-lat_d)).*180/pi

x=sin(lng_o-lng_d).*cos(lat_o);
y=cos(lat_d).*sin(lat_o)-sin(lat_d).*cos(lat_o).*cos(lng_o-lng_d);
heading_val2=mod(((atan(x./y).*180/pi)+360),360)