import osmtogeojson from 'osmtogeojson'
import queryoverpass from 'query-overpass'
import convert from 'xml-js'


var map;

var cph = {
  lat: -33.8688,
  lng: 151.2195
};
var tivoli = {
  lat: 55.67405678196548,
  lng: 12.568788199999972,
};
var sydney={
        lat: -33.8688,
        lng: 151.2195
  };

var markers = [];
var markers_SV=[];
var all_overlays = [];
var headings=[];
var lats=[];
var lngs=[];

localStorage.clear();

//--------------------------------------------------------------------
function openInNewTab(map) {

  localStorage.clear();

  var mapSettings = {
    "center": map.getCenter(),
    "zoom": map.getZoom(),
    "heading": map.getHeading(),
    "tilt": map.getTilt(),
    "disableDefaultUI": "true",
    "mapTypeId": google.maps.MapTypeId.SATELLITE,
    "styles": [{
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "administrative.land_parcel",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "administrative.neighborhood",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "poi",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "road",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "transit",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "water",
        "elementType": "labels.text",
        "stylers": [{
          "visibility": "off"
        }]
      }
    ]
  };

  getheadings();

  current_lng= map.getCenter().lng();
  current_lat=map.getCenter().lat();

  localStorage.setItem('mapSettings', JSON.stringify(mapSettings));
  localStorage.setItem('current_lng',JSON.stringify(current_lng));
  localStorage.setItem('current_lat',JSON.stringify(current_lat));

  //open the new tab which loads the json settings
  //cannot directly call new google maps link with correct settings because variables cant seem to be added to the non-static image ur/
  var win = window.open('pre_export.html', 'Export');
  win.focus();
}
//------------------------------------------------------------------------------------------------------------------


//DROPDOWN
//Sets the zoom level depending on the dropdown
function listQ() {
  var e = document.getElementById("select");
  if ("world3" === e.options[e.selectedIndex].value) {
    map.setZoom(3);
  } else if ("world4" === e.options[e.selectedIndex].value) {
    map.setZoom(4);
  } else if ("country5" === e.options[e.selectedIndex].value) {
    map.setZoom(5);
  } else if ("country6" === e.options[e.selectedIndex].value) {
    map.setZoom(6);
  } else if ("state" === e.options[e.selectedIndex].value) {
    map.setZoom(7);
  } else if ("region8" === e.options[e.selectedIndex].value) {
    map.setZoom(8);
  } else if ("region9" === e.options[e.selectedIndex].value) {
    map.setZoom(9);
  } else if ("region10" === e.options[e.selectedIndex].value) {
    map.setZoom(10);
  } else if ("region11" === e.options[e.selectedIndex].value) {
    map.setZoom(11);
  } else if ("city12" === e.options[e.selectedIndex].value) {
    map.setZoom(12);
  } else if ("city13" === e.options[e.selectedIndex].value) {
    map.setZoom(13);
  } else if ("city14" === e.options[e.selectedIndex].value) {
    map.setZoom(14);
  } else if ("city15" === e.options[e.selectedIndex].value) {
    map.setZoom(15);
  } else if ("city16" === e.options[e.selectedIndex].value) {
    map.setZoom(16);
  } else if ("city17" === e.options[e.selectedIndex].value) {
    map.setZoom(17);
  } else if ("site" === e.options[e.selectedIndex].value) {
    map.setZoom(18);
  }
}


document.getElementById("select").addEventListener("click", listQ);


//CENTER MAP BUTTONS
function CenterControl(controlDiv, map, center) {
  var control = this;

  // Set the center property upon construction
  control.center_ = center;
  controlDiv.style.clear = 'both';

  // Set CSS for the control border
  var goCenterUI = document.createElement('div');
  goCenterUI.id = 'goCenterUI';
  goCenterUI.title = 'Click to recenter the map';
  controlDiv.appendChild(goCenterUI);

  // Set CSS for the control interior
  var goCenterText = document.createElement('div');
  goCenterText.id = 'goCenterText';
  goCenterText.innerHTML = 'Center Map';
  goCenterUI.appendChild(goCenterText);

  // Set CSS for the setCenter control border
  var setCenterUI = document.createElement('div');
  setCenterUI.id = 'setCenterUI';
  setCenterUI.title = 'Click to change the center of the map';
  controlDiv.appendChild(setCenterUI);

  // Set CSS for the control interior
  var setCenterText = document.createElement('div');
  setCenterText.id = 'setCenterText';
  setCenterText.innerHTML = 'Set Center';
  setCenterUI.appendChild(setCenterText);

  // Set up the click event listener for 'Center Map': Set the center of
  // the map
  // to the current center of the control.
  goCenterUI.addEventListener('click', function() {
    var currentCenter = control.getCenter();
    map.setCenter(currentCenter);
  });

  // Set up the click event listener for 'Set Center': Set the center of
  // the control to the current center of the map.
  setCenterUI.addEventListener('click', function() {
    var newCenter = map.getCenter();
    control.setCenter(newCenter);
  });
}



//BORDERS BUTTON
function bordersControl(bordersDiv, map) {
  var control = this;

  // Set the center property upon construction
  control.center_ = center;
  bordersDiv.style.clear = 'both';

  // Set CSS for the control border
  var bordersUI = document.createElement('div');
  bordersUI.id = 'goCenterUI';
  bordersUI.title = 'Click to recenter the map';
  bordersDiv.appendChild(bordersUI);

  // Set CSS for the control interior
  var bordersText = document.createElement('div');
  bordersText.id = 'goCenterText';
  bordersText.innerHTML = 'Borders';
  bordersUI.appendChild(bordersText);

  // Set up the click event listener for 'Set Center': Set the center of
  // the control to the current center of the map.
  bordersUI.addEventListener('click', function() {
    var newCenter = map.getCenter();
    control.setCenter(newCenter);
  });
};



// /**
//  * Define a property to hold the center state.
//  * @private
//  */
CenterControl.prototype.center_ = null;

// /**
//  * Gets the map center.
//  * @return {?google.maps.LatLng}
//  */
CenterControl.prototype.getCenter = function() {
  return this.center_;
};

// /**
//  * Sets the map center.
//  * @param {?google.maps.LatLng} center
//  */
CenterControl.prototype.setCenter = function(center) {
  this.center_ = center;
};






//-------------------------------------------------------------------------------------------------------------------------------
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

function initAutocomplete() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: tivoli,
    mapTypeId: google.maps.MapTypeId.SATELLITE,
    zoom: 12,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DEFAULT,
      mapTypeIds: ['roadmap', 'terrain', 'satellite']
    },
    styles: [{
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "administrative.land_parcel",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "administrative.neighborhood",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "poi",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "road",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "transit",
        "stylers": [{
          "visibility": "off"
        }]
      },
      {
        "featureType": "water",
        "elementType": "labels.text",
        "stylers": [{
          "visibility": "off"
        }]
      }
    ]
  });



  // var drawingManager = new google.maps.drawing.DrawingManager({
  //           drawingMode: google.maps.drawing.OverlayType.MARKER,
  //           drawingControl: true,
  //           drawingControlOptions: {
  //             position: google.maps.ControlPosition.TOP_RIGHT,
  //             drawingModes: ['marker', 'circle', 'polygon', 'polyline', 'rectangle']
  //           },
  //           markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
  //           circleOptions: {
  //             fillColor: '#ffff00',
  //             fillOpacity: 1,
  //             strokeWeight: 5,
  //             clickable: false,
  //             editable: true,
  //             zIndex: 1
  //           }
  //         });
  //
  //         drawingManager.setMap(map);

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });


  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }

      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };


      var whitecircle = {
        path: 'M2,12C2,6.48,6.48,2,12,2s10,4.48,10,10s-4.48,10-10,10S2,17.52,2,12z M12,18c3.31,0,6-2.69,6-6s-2.69-6-6-6s-6,2.69-6,6 S8.69,18,12,18z',
        fillColor: 'white',
        fillOpacity: 1,
        scale: 1,
        strokeColor: 'white',
        strokeWeight: 1,
        anchor: new google.maps.Point(12, 12)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: whitecircle,
        title: place.name,
        position: place.geometry.location,
        draggable: true
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });


  var centerControlDiv = document.createElement('div');
  var centerControl = new CenterControl(centerControlDiv, map, cph);

  centerControlDiv.index = 1;
  centerControlDiv.style['padding-top'] = '10px';
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);




  //var bordersDiv = document.createElement('div');
  //var bordersControl = new bordersControl(bordersDiv, map);

  //bordersDiv.index = 1;
  //bordersDiv.style['padding-top'] = '10px';
  //map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(bordersDiv);



  //if the center of the map changes then move the white marker
  map.addListener('center_changed', function() {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setPosition(map.getCenter());
}
  });

//if the white marker center changes, then autmatically set the new center to the marker position
// marker.addListener('dragend', function() {
//
//     map.setCenter(marker.getPosition());
//   });


  // This event listener will call addMarker() when the map is clicked.
  map.addListener('click', function(event) {
    addMarker(event.latLng);
  });

  // map.addListener(drawingManager, 'overlaycomplete', function(e) {
  //     all_overlays.push(e);
  //     if (e.type != google.maps.drawing.OverlayType.MARKER) {
  //     // Switch back to non-drawing mode after drawing a shape.
  //     drawingManager.setDrawingMode(null);
  //     // Add an event listener that selects the newly-drawn shape when the user
  //     // mouses down on it.
  //     var newShape = e.overlay;
  //     newShape.type = e.type;
  //   map.addListener(newShape, 'click', function() {
  //       setSelection(newShape);
  //     });
  //     setSelection(newShape);
  //   }
  // });

}//function initAutocomplete
window.initAutocomplete = initAutocomplete;
//---------------------------------------------------------------------------------------------------------------





//---------------------------------------------------------------------------------------------------------------
//FUNCTIONS

//print current position in a div
// function getCurrent(map) {
//   var currentPosition = map.getCenter();
//   document.getElementById("demo").innerHTML = currentPosition;
// }


// Adds a marker to the map and push to the array.
function addMarker(location) {
  var marker = new google.maps.Marker({
    position: location,
    map: map
  });
  markers_SV.push(marker);
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < markers_SV.length; i++) {
    markers_SV[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() {
  setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers_SV = [];
}

function printlatlng() {
  for (var i = 0; i < markers_SV.length; i++) {
    markers_SV[i].setMap(map);
    var newDiv = document.createElement("div");
    var newContent = document.createTextNode(markers_SV[i].getPosition().lat(),markers_SV[i].getPosition().lng());
    newDiv.appendChild(newContent);
    document.body.appendChild(newDiv);
  }
}

function deleteAllShape() {
  for (var i=0; i < all_overlays.length; i++)
  {
    all_overlays[i].overlay.setMap(null);
  }
  all_overlays = [];
}



function getheadings(){
//given the location af the marker and the location of the site, calculate the heading vector

for (var i = 0; i < markers_SV.length; i++) {
  markers_SV[i].setMap(map);

  var lat_d=markers_SV[i].getPosition().lat();
  var lat_o=map.getCenter().lat();
  var lng_d=markers_SV[i].getPosition().lng();
  var lng_o=map.getCenter().lng();

  var y = Math.sin(lng_o-lng_d) * Math.cos(lat_o);
  var x = Math.cos(lat_d)*Math.sin(lat_o) - Math.sin(lat_d)*Math.cos(lat_o)*Math.cos(lng_o-lng_d);
  var brng = ((Math.atan2(y, x)*(180/Math.PI))+360)%360;


  headings.push(brng);
  lats.push(lat_d);
  lngs.push(lng_d);

  // var newDiv = document.createElement("div");
  // var newContent = document.createTextNode(brng);
  // newDiv.appendChild(newContent);
  // document.body.appendChild(newDiv);
}


localStorage.setItem('headings', JSON.stringify(headings));
localStorage.setItem('lats', JSON.stringify(lats));
localStorage.setItem('lngs', JSON.stringify(lngs));

// var newDiv = document.createElement("div");
// var newContent = document.createTextNode(headings.length);
// newDiv.appendChild(newContent);
// document.body.appendChild(newDiv);
}; //getheadings



function initWikimapia(){
  getMapia_xml();
}
window.initWikimapia = initWikimapia;


function getMapia_xml(){

  var map_bound = map.getBounds();
  //establish wikimapia url
  //http://api.wikimapia.org/?function=box&bbox=12.56779343,55.6715685,12.5737642,55.6764015format=json&key=example
  var mapia_url='http://api.wikimapia.org/?function=box&bbox='+map_bound.b.b+','+map_bound.f.b+','+map_bound.b.f+','+map_bound.f.f+'format=json&key=example'
  console.log(mapia_url)

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      myFunction(this);
    }
  };
  xhttp.open("GET", mapia_url, true);
  xhttp.send();
}//getMapia_xml

function myFunction(xml){
  var xmlDoc = xml.responseXML;
  // console.log(xmlDoc)

  var xmlText = new XMLSerializer().serializeToString(xmlDoc);

  var result1=convert.xml2json(xmlText,{compact: true, spaces:4});
  var test3=JSON.parse(result1);
  // console.log(test3)

  var newtest=extract(test3);
  // console.log(newtest)
  var final_geo=initiate_geojsonmapbox(newtest);
  console.log(final_geo)
//add data layer

  // var coords = final_geo.source.data.features[0].geometry.coordinates[0];
  // console.log(coords);
  //
  // var areas=[];
  // for (var i in final_geo.source.data.features){
  //   areas.push(final_geo.source.data.features[i].geometry.coordinates[0])
  // };
  // console.log(areas)

  // console.log(final_geo.source.data);
  // map.data.add({geometry: new google.maps.Data.Polygon(areas)});
  map.data.addGeoJson(final_geo.source.data);
  map.data.setStyle(final_geo.paint);

};//myFunction


function extract(json_input){
  var geojson_output=[];

 for (var i in json_input.folder.place) {
   geojson_output.push({'type':'Feature'});
   geojson_output[i].properties={'name': json_input.folder.place[i].name._text};
   geojson_output[i].geometry={'type':'Polygon','coordinates':[[]]}
   for (var j in json_input.folder.place[i].polygon.point){
     var current = json_input.folder.place[i].polygon.point[j]._attributes;
     // geojson_output[i].geometry.coordinates[0].push({lat:parseFloat(current.y), lng:parseFloat(current.x)});
      geojson_output[i].geometry.coordinates[0].push([parseFloat(current.x), parseFloat(current.y)]);
   };
   //add last point as first point
   var first_coord=geojson_output[i].geometry.coordinates[0][0];
   // console.log(first_coord)
   geojson_output[i].geometry.coordinates[0].push(first_coord);
 };
 return geojson_output
};//extract

function initiate_geojsonmapbox(geojson_input){
  var finalgeo={
      "id": "wikimapia",
      "type": "fill",
      "source": {
          "type": "geojson",
          "data": {
            "type": "FeatureCollection",
            "features": geojson_input,
          },
        },
      "layout": {},
      'paint':{
        // 'fill-color': '#088',
        // 'fill-opacity': 0.5
        fillColor: '#77b2e9',
        fillOpacity: 0.5,
        strokeColor: '#3484d9',
        strokeWeight: 2,
        strokeOpacity:1
      }
  };

  return finalgeo;

};//initiate_geojsonmapbox
