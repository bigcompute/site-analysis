
// var tivoli = {
//   lat: 55.67405678196548,
//   lng: 12.568788199999972,
// };

// import html2canvas from 'html2canvas'

var panorama;
var panoramas=[];
var pano_IDs=[];
var map;
var latlng;

//load data passed from index.html
var headings = JSON.parse(localStorage.getItem("headings"));
var current_lat = JSON.parse(localStorage.getItem("lats"));
var current_lng = JSON.parse(localStorage.getItem("lngs"));


function initialize() {
  initMap();
  initStreet();
}

function initMap() {
  var imported_settings = localStorage.getItem('mapSettings');
  //console.log('Map Settings: ' + JSON.parse(imported_settings));

  map1 = new google.maps.Map(document.getElementById('map1'), JSON.parse(imported_settings));
  map1.setZoom(5);

  map2 = new google.maps.Map(document.getElementById('map2'), JSON.parse(imported_settings));
  map2.setZoom(7);

  map3 = new google.maps.Map(document.getElementById('map3'), JSON.parse(imported_settings));
  map3.setZoom(8);

  map4 = new google.maps.Map(document.getElementById('map4'), JSON.parse(imported_settings));
  map4.setZoom(14);

  map5 = new google.maps.Map(document.getElementById('map5'), JSON.parse(imported_settings));
  map5.setZoom(17);

  map6 = new google.maps.Map(document.getElementById('map6'), JSON.parse(imported_settings));
  map6.setZoom(18);
  map6.setTilt(45);

  map7 = new google.maps.Map(document.getElementById('map7'), JSON.parse(imported_settings));
  map7.setZoom(18);
  map7.setTilt(45);
  map7.setHeading(90);

  map8 = new google.maps.Map(document.getElementById('map8'), JSON.parse(imported_settings));
  map8.setZoom(18);
  map8.setTilt(45);
  map8.setHeading(180);

  map9 = new google.maps.Map(document.getElementById('map9'), JSON.parse(imported_settings));
  map9.setZoom(18);
  map9.setTilt(45);
  map9.setHeading(270);
}

function initStreet() {
  var sv = new google.maps.StreetViewService();
  for (var i = 0; i < headings.length; i++) {
      var newDiv2 = document.createElement("div");
      newDiv2.id="streetview_"+i;
      document.getElementById('map12').appendChild(newDiv2);
      newDiv2.classList.add('street');

      //retrieve the nearest pano ID using StreetViewService


      panorama = new google.maps.StreetViewPanorama(
                    newDiv2,{
                      visible: true,
                      linksControl: false,
                      addressControl: false,
                      fullscreenControl: false
                    });
      panoramas.push(panorama);

      sv.getPanorama({location: {
        "lat": current_lat[i],
        "lng": current_lng[i],
      }, radius: 50}, processSVData);


//------DEBUGGING
      // var newDiv1 = document.createElement("div");
      // var newContent1 = document.createTextNode(headings[i]);
      // newDiv1.appendChild(newContent1);
      // document.getElementById('map12').appendChild(newDiv1);
  }
}

function processSVData(data, status) {
  if (status === 'OK') {

      pano_IDs.push(data.location.pano)

      if (pano_IDs.length==headings.length){
          for (var i = 0; i < headings.length; i++) {
                panoramas[i].setPano(pano_IDs[i]);
                panoramas[i].setPov({heading: headings[i],pitch:10})
            }
      }
      //panoramas.push(data.location.pano)
    } else if (status=== 'ZERO_RESULTS'){
      console.error('Zero results.');
    } else if (status==='UNKNOWN_ERROR'){
    console.error('Unknown error.');
  }
}

// html2canvas(document.body.div("map12")).then(function(canvas) {
//     document.body.appendChild(canvas);
// });

// html2canvas(document.body).then(function(canvas) {
//     // Export the canvas to its data URI representation
//     var base64image = canvas.toDataURL("image/png");
//
//     // Open the image in a new window
//     window.open(base64image , "_blank");
// });
